var Game = {
    three: {
        scene: null,
        camera: null,
        cameraHelper: null,
        renderer: null
    },
    objects: [
        {
            obj: null,
            initialise: function () {
                var imgTexture = THREE.ImageUtils.loadTexture( "textures/lavatile.jpg" );
                imgTexture.repeat.set( 4, 2 );
                imgTexture.wrapS = imgTexture.wrapT = THREE.RepeatWrapping;
                imgTexture.anisotropy = 16;

                var geometry = new THREE.SphereBufferGeometry( 35, 35, 35 );
                var material = new THREE.MeshLambertMaterial( { map: imgTexture, color: 0x00ff00 } );
                this.obj = new THREE.Mesh( geometry, material );
                Game.three.scene.add( this.obj );
            },
            onRender: function () {
                this.obj.rotation.x += 0.05;

                var timer = Date.now() * 0.00125;
                this.obj.position.x = Math.cos( timer ) * 200;
                this.obj.position.y = Math.sin( timer ) * 200;
            }
        },
        {
            obj: null,
            initialise: function () {
                var imgTexture = THREE.ImageUtils.loadTexture( "textures/lavatile.jpg" );
                imgTexture.repeat.set( 4, 2 );
                imgTexture.wrapS = imgTexture.wrapT = THREE.RepeatWrapping;
                imgTexture.anisotropy = 16;

                var material = new THREE.MeshLambertMaterial( { map: imgTexture, color: 0xff0000 } );
                var geometry = new THREE.SphereBufferGeometry( 70, 32, 16 );

                this.obj = new THREE.Mesh( geometry, material );
                Game.three.scene.add( this.obj );
            },
            onRender: function () {
                this.obj.rotation.z -= 0.01;
            }
        },
        {
            obj: null,
            initialise: function () {
                var imgTexture = THREE.ImageUtils.loadTexture( "textures/lavatile.jpg" );
                imgTexture.repeat.set( 4, 2 );
                imgTexture.wrapS = imgTexture.wrapT = THREE.RepeatWrapping;
                imgTexture.anisotropy = 16;

                var material = new THREE.MeshLambertMaterial( { map: imgTexture, color: 0x335555 } );
                var geometry = new THREE.SphereBufferGeometry( 70, 32, 16 );

                this.obj = new THREE.Mesh( geometry, material );
                Game.three.scene.add( this.obj );
                this.obj.position.x = 300;
                this.obj.position.y = 300;
            },
            onRender: function () {
                this.obj.rotation.z -= 0.01;
            }
        },
        {
            obj: null,
            initialise: function () {
                var imgTexture = THREE.ImageUtils.loadTexture( "textures/lavatile.jpg" );
                imgTexture.repeat.set( 4, 2 );
                imgTexture.wrapS = imgTexture.wrapT = THREE.RepeatWrapping;
                imgTexture.anisotropy = 16;

                var material = new THREE.MeshLambertMaterial( { map: imgTexture, color: 0x4455ff } );
                var geometry = new THREE.SphereBufferGeometry( 70, 32, 16 );

                this.obj = new THREE.Mesh( geometry, material );
                Game.three.scene.add( this.obj );
                this.obj.position.x = 500;
                this.obj.position.y = 0;
            },
            onRender: function () {
                this.obj.rotation.z -= 0.01;
            }
        }
    ],
    initialise: function ($elem) {
        var width = $elem.innerWidth(),
            height = $elem.innerHeight();

        Game.three.scene = new THREE.Scene();
        Game.three.camera = new THREE.PerspectiveCamera( 75, width / height, 0.1, 2000 );
        Game.three.renderer = new THREE.WebGLRenderer({ antialias: true });

        Game.three.cameraHelper = new THREE.CameraHelper(Game.three.camera);
        Game.three.scene.add( Game.three.cameraHelper );

        Game.three.renderer.setSize( width, height );
        Game.three.renderer.setClearColor( 0x0a0a0a );
        Game.three.renderer.setPixelRatio( window.devicePixelRatio );
        Game.three.renderer.sortObjects = true;
        $elem.append( Game.three.renderer.domElement );
        Game.three.renderer.gammaInput = true;
        Game.three.renderer.gammaOutput = true;

        for ( var i = 0; i < Game.objects.length; i++ ) {
            Game.objects[i].initialise();
        }

        Game.three.camera.position.set( 100, 600, 0 );
        Game.three.camera.lookAt( Game.three.scene.position );

        var particleLight = new THREE.Mesh( new THREE.SphereBufferGeometry( 4, 8, 8 ), new THREE.MeshBasicMaterial( { color: 0xffffff } ) );
        Game.three.scene.add( particleLight );

        Game.three.scene.add( new THREE.AmbientLight( 0x444444 ) );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
        directionalLight.position.set( 1, 1, 1 ).normalize();
        Game.three.scene.add( directionalLight );

        var pointLight = new THREE.PointLight( 0xffffff, 2, 800 );
        particleLight.add( pointLight );

        Game.bindEvents($elem);

        Game.render();
    },
    render: function () {
        requestAnimationFrame( Game.render );

        for ( var i = 0; i < Game.objects.length; i++ ) {
            Game.objects[i].onRender();
        }

        Game.three.cameraHelper.update();
        Game.three.cameraHelper.visible = true;

        Game.three.renderer.render( Game.three.scene, Game.three.camera );

        Game.console();
    },
    bindEvents: function ($elem) {
        document.onkeydown = Game.keyboard;
        $elem.mousemove(function (e) {
            Game.mouse.mouseMove(e);
        });
        $elem.mouseleave(function () {
            Game.mouse.mouseLeave();
        });
        Game.movement.doMovement();
    },
    console: function () {
        $('#overlay-x').text(Math.round(Game.three.camera.position.x));
        $('#overlay-y').text(Math.round(Game.three.camera.position.y));
        $('#overlay-z').text(Math.round(Game.three.camera.position.z));

        $('#overlay-lr').text(Game.three.camera.rotation.y.toFixed(3));
        $('#overlay-ud').text(Game.three.camera.rotation.x.toFixed(3));

        var mx = Game.movement.turnY.toFixed(3);
        var my = Game.movement.turnX.toFixed(3);
        $('#overlay-xy').text(mx + ', ' + my);

        $('#overlay-speed').text(Game.movement.moveZ.toFixed(1));
    },
    movement: {
        turnY: 0,
        turnX: 0,
        moveZ: 0,
        doMoveZ: function (z) {
            Game.three.camera.translateZ(-z);
        },
        doTurnY: function (y) {
            Game.three.camera.rotateOnAxis( new THREE.Vector3( 0, y, 0 ), 0.050 );
        },
        doTurnX: function (x) {
            Game.three.camera.rotateOnAxis( new THREE.Vector3( x, 0, 0 ), 0.050 );
        },
        doMovement: function () {
            // Turn left/right
            if (Math.abs(Game.movement.turnY) > 5) {
                Game.movement.doTurnY(Game.movement.turnY / 100);
            }

            // Turn up/down
            if (Math.abs(Game.movement.turnX) > 5) {
                Game.movement.doTurnX(Game.movement.turnX / 100);
            } else {
                // Drift to 0
                //Game.movement.levelAxis();
            }

            // Move backwards/forwards
            if (Math.abs(Game.movement.moveZ) > 0) {
                Game.movement.doMoveZ(Game.movement.moveZ);
                Game.movement.slowDown();
            }

            setTimeout(function () {
                Game.movement.doMovement();
            }, 10);

            // Keep one axis level for ease of control
            Game.three.camera.rotation.z = 0;
        },
        slowDown: function () {
            if (Game.movement.moveZ > 0) {
                Game.movement.moveZ = Game.movement.moveZ - 0.01;
            } else if (Game.movement.moveZ < 0) {
                Game.movement.moveZ = Game.movement.moveZ + 0.01;
            }
        },
        levelAxis: function () {
            if (Game.three.camera.rotation.x > 0) {
                Game.movement.doTurnX(-0.1);
            } else if (Game.three.camera.rotation.x < 0) {
                Game.movement.doTurnX(0.1);
            }
        }
    },
    keyboard: function (e) {
        e = e || window.event;

        if (e.keyCode == '87') {
            // W Arrow
            if (Game.movement.moveZ <= 3) {
                Game.movement.moveZ++;
            }
        }
        else if (e.keyCode == '83') {
            // S Arrow
            if (Game.movement.moveZ >= -3) {
                Game.movement.moveZ--;
            }
        }
    },
    mouse: {
        mouseMove: function (e) {
            var $target = $( e.target );
            var x = $target.width() / 2 - e.pageX;
            var y = $target.height() / 2 - e.pageY;

            Game.movement.turnY = (x / $target.width()) * 100;
            Game.movement.turnX = (y / $target.height()) * 100;
        },
        mouseLeave: function () {
            Game.movement.turnY = 0;
            Game.movement.turnX = 0;
        }
    }
};